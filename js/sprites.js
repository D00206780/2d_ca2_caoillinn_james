class Sprite {

    constructor(properties, artist, behaviours) {
        this.x = properties.x;
        this.y = properties.y;
        this.width = properties.width;
        this.height = properties.height;
        this.artist = artist || null;
        this.behaviours = behaviours || [];
    }

    draw(context) {
        if (this.artist !== null) {
            this.artist.draw(this, context);
        } else {
            context.drawImage(
                this.x, this.y,
                this.width, this.height,
            );
        }
    }

    update(now) {
        for (let i = 0; i < this.behaviours.length; ++i) {
            this.behaviours[i].execute(this, now);
        }
    }
}

class SpriteSheetArtist {

    constructor(spriteSheet, cells) {
        this.spriteSheet = spriteSheet;
        this.cellIndex = 0;
        this.cells = cells;
    }

    draw(sprite, context) {
        let cell = this.cells[this.cellIndex];
        context.drawImage(this.spriteSheet,
            cell.left, cell.top,
            cell.width, cell.height,
            sprite.x, sprite.y,
            sprite.width, sprite.height,
        );
    }

    advance() {
        if (this.cellIndex === this.cells.length - 1) {
            this.cellIndex = 0;
        } else {
            this.cellIndex++;
        }
    }

    retreat() {
        if (this.cellIndex === 0) {
            this.cellIndex = this.cells.length - 1;
        } else {
            this.cellIndex--;
        }
    }
}
