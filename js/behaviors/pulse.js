class PulseBehaviour {

    constructor(duration) {
        this.stopwatch = new Stopwatch();
        this.opacityThreshold = 0.5;
        this.duration = duration;
        this.lastCycleTime = 0;
        this.pulsating = false;
        this.execute();
    }

    dim(element, elapsed) {
        element.opacity = 1 - ((1 - this.opacityThreshold) * (parseFloat(elapsed) / this.duration));
    }

    brighten(element, elapsed) {
        element.opacity += 1 * (1 - this.opacityThreshold) * parseFloat(elapsed) / this.duration;
    }

    startPulsing() {
        this.pulsating = true;
        this.stopwatch.start();
    }

    resetTimer() {
        this.stopwatch.stop();
        this.stopwatch.reset();
        this.stopwatch.start();
    }

    execute(element, now) {

        if (!this.pulsating) {
            this.startPulsing();
        } else {
            let elapsed = this.stopwatch.getElapsedTime();

            if (this.stopwatch.getElapsedTime(now) > this.duration) {
                this.resetTimer();
                return;
            }

            if (elapsed < this.duration / 2) {
                this.dim(element, elapsed);
            }
            else {
                this.brighten(element, elapsed);
            }
        }
    }
}