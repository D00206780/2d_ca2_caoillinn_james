//Cube object (playable character)
class Cube {

    constructor() {
        this.x = null;
        this.y = null;
    }

    //Get the cube x, y
    get() {
        return ({
            x: this.x,
            y: this.y
        });
    }

    //Set the cube x, y
    set(x, y) {
        this.x = x
        this.y = y;
    }

    //Set the cube x, y
    setPosition(position) {
        this.x = position.x;
        this.y = position.y;
    }
};