//Grid object
class Grid {

    //Construct the grid
    constructor() {
        this.backgroundBlockColor = null;
        this.exitPosition = null;
        this.width = null;
        this.height = null;
        this.grid = [];
    }

    //Initialise the grid
    init(columns, rows, color) {
        this.width = columns;
        this.height = rows;
        for (let x = 0; x < columns; x++) {
            this.grid.push([]);
            for (let y = 0; y < rows; ++y) {
                this.grid[x].push(color);
            }
        }
    }

    //Set the block at a given coordinate to a given color
    set(x, y, color) {
        this.grid[x][y] = color;
    }

    //Set the block at a given position to a given color
    setBlock(position, color) {
        this.grid[position.x][position.y] = color;
    }

    //Set the position of the exit block
    setExitPosition(x, y) {
        this.exitPosition = {
            x: x,
            y: y,
        }
    }

    //Get the block at a given coordinate 
    get(x, y) {
        if (this.grid[x][y] != null) {
            return this.grid[x][y];
        }
    }

    //Get the block at a given position
    getBlock(position) {
        if (this.grid[position.x][position.y] != null) {
            return this.grid[position.x][position.y];
        }
    }

    //Get the positon of the exit block
    getExitPosition() {
        return this.exitPosition;
    }

    //Set the background block color
    setBackgroundBlockColor(color) {
        this.backgroundBlockColor = color;
    }

    //Get the background block color
    getBackgroundBlockColor() {
        return this.backgroundBlockColor;
    }
};