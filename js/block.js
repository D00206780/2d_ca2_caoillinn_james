class Block {

    constructor(properties, color, behaviours) {
        this.opacity = 1;
        this.color = color;
        this.x = properties.x;
        this.y = properties.y;
        this.width = properties.width;
        this.height = properties.height;
        this.behaviours = behaviours || [];
    }

    draw(context) {
        context.save();
        context.fillStyle = this.color;
        context.globalAlpha = this.opacity;
        context.fillRect(this.x, this.y, this.width, this.height);
        context.restore();
    }

    update(now) {
        for (let i = 0; i < this.behaviours.length; ++i) {
            this.behaviours[i].execute(this, now);
        }
    }
}